import 'package:flutter/material.dart';
import 'package:flutter_business_1907a/bean/detail_goods_bean_result.dart';
import 'package:flutter_business_1907a/page/detail/detail_bottom.dart';
import 'package:flutter_business_1907a/page/detail/detail_explain.dart';
import 'package:flutter_business_1907a/page/detail/detail_tab_bar.dart';
import 'package:flutter_business_1907a/page/detail/detail_top.dart';
import 'package:flutter_business_1907a/provider/detail_info_provider.dart';
import 'package:flutter_business_1907a/service/service_method.dart';
import 'package:provider/provider.dart';

class DetailsPage extends StatelessWidget {
  final String goodsId;

  const DetailsPage({Key key, this.goodsId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _getBackInfo(context, goodsId);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text('商品详情'),
      ),
      body: Stack(
        children: [
          ListView(
            children: [
              DetailTop(),
              DetailExplain(),
              DetailTabBar(),
            ],
          ),
          Positioned(
            child: DetailBottom(),
            bottom: 0,
            left: 0,
          )
        ],
      ),
      // FutureBuilder(
      //   future: _getBackInfo(context, goodsId),
      //   builder: (context, val) {
      //     print(val.data);
      //     if (val.hasData) {
      //       print('hasData');
      //       return Stack(
      //         children: [
      //           ListView(
      //             children: [
      //               DetailTop(),
      //             ],
      //           )
      //         ],
      //       );
      //     } else {
      //       return Text('加载中...');
      //     }
      //   },
      // ),
    );
  }

  Future _getBackInfo(context, id) async {
    return request('getGoodsDetail', id, true).then((value) {
      Provider.of<DetailInfoProvider>(context, listen: false)
          .setGoodsInfo(DetailGoodsBeanResult.fromJson(value).data);
    });
  }
}
