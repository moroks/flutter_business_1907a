// const baseUrl = 'http://192.168.1.1';//开发测试线下环境
// const baseUrl = 'http://202.65.52.34';//开发测试线上环境
const baseUrl = 'http://www.fenghongzhang.com:9999'; //生产环境

const servicePath = {
  //商城首页的轮播提
  'homePageBanner': baseUrl + '/banner/getImages',
  //商城首页导航
  'homeNavigator': baseUrl + '/attention/all',
  //商城首页商品推荐
  'homeRecommend': baseUrl + '/goods/category/15/1/10',
  //商城首页火爆专区
  'homeHotGoods': baseUrl + '/goods/category/14/1/',

  //分类页面一级分类
  'getGoodsParent': baseUrl + '/category/getType',
  //分类页面三级分类列表
  'categoryGoodsList': baseUrl + '/goods/category/',
  //详情接口
  'getGoodsDetail': baseUrl + '/goods/info/',
};
