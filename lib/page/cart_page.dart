import 'package:flutter/material.dart';
import 'package:flutter_business_1907a/bean/cart_info_bean_result.dart';
import 'package:flutter_business_1907a/provider/cart_provider.dart';
import 'package:flutter_business_1907a/provider/counter.dart';
import 'package:provider/provider.dart';

class CartPage extends StatelessWidget {

  void _getCartInfo(context) async{
    await Provider.of<CartProvider>(context).getCartInfo();
  }


  @override
  Widget build(BuildContext context) {
    _getCartInfo(context);
    return Scaffold(
      appBar: AppBar(title: Text('购物车'),),
      body:ListView.builder(
        itemCount: Provider.of<CartProvider>(context).cartList.length,
          itemBuilder: (context,index){
        return ListTile(title: Text('商品名称:${Provider.of<CartProvider>(context).cartList[index].goodsName}'),);
      })
    );
  }
}
