import 'dart:convert' show json;

class CategoryParentBeanResult {

  int code;
  String msg;
  List<CategoryParentBean> data;

  CategoryParentBeanResult.fromParams({this.code, this.msg, this.data});

  factory CategoryParentBeanResult(jsonStr) => jsonStr == null ? null : jsonStr is String ? new CategoryParentBeanResult.fromJson(json.decode(jsonStr)) : new CategoryParentBeanResult.fromJson(jsonStr);

  CategoryParentBeanResult.fromJson(jsonRes) {
    code = jsonRes['code'];
    msg = jsonRes['msg'];
    data = jsonRes['data'] == null ? null : [];

    for (var dataItem in data == null ? [] : jsonRes['data']){
      data.add(dataItem == null ? null : new CategoryParentBean.fromJson(dataItem));
    }
  }

  @override
  String toString() {
    return '{"code": $code,"msg": ${msg != null?'${json.encode(msg)}':'null'},"data": $data}';
  }
}

class CategoryParentBean {

  Object categoryIcon;
  int id;
  int parentId;
  String categoryName;

  CategoryParentBean.fromParams({this.categoryIcon, this.id, this.parentId, this.categoryName});

  CategoryParentBean.fromJson(jsonRes) {
    categoryIcon = jsonRes['categoryIcon'];
    id = jsonRes['id'];
    parentId = jsonRes['parentId'];
    categoryName = jsonRes['categoryName'];
  }

  @override
  String toString() {
    return '{"categoryIcon": $categoryIcon,"id": $id,"parentId": $parentId,"categoryName": ${categoryName != null?'${json.encode(categoryName)}':'null'}}';
  }
}

