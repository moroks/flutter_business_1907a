import 'package:fluro/fluro.dart';
import 'package:flutter_business_1907a/router/detail_handler.dart';

class Routers{
  static String root='/';//根目录
  static String detailsPage = '/detail';
  static void configureRoutes(FluroRouter router){
    router.notFoundHandler = Handler(
      handlerFunc: (context,params){
        print('ERROR=================>当前Router没有找到');
        return null;
      }
    );
    router.define(detailsPage, handler: detailHandler);
  }
}