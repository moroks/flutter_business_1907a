import 'package:flutter/material.dart';
import 'package:flutter_business_1907a/provider/cart_provider.dart';
import 'package:flutter_business_1907a/provider/detail_info_provider.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';

class DetailBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(750),
      color: Colors.white,
      height: ScreenUtil().setHeight(80),
      child: Row(
        children: [
          InkWell(
            onTap: () {},
            child: Container(
              width: ScreenUtil().setWidth(110),
              alignment: Alignment.center,
              child: Icon(
                Icons.shopping_cart,
                size: 35,
                color: Colors.red,
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Provider.of<CartProvider>(context, listen: false).save(
                  Provider.of<DetailInfoProvider>(context, listen: false).goodsInfo.id,
                  Provider.of<DetailInfoProvider>(context, listen: false).goodsInfo.goodsDesc,
                  1,
                  Provider.of<DetailInfoProvider>(context, listen: false)
                      .goodsInfo
                      .goodsDefaultPrice,
                  Provider.of<DetailInfoProvider>(context, listen: false)
                      .goodsInfo
                      .goodsDefaultIcon);
            },
            child: Container(
              alignment: Alignment.center,
              width: ScreenUtil().setWidth(320),
              color: Colors.green,
              child: Text(
                '加入购物车',
                style: TextStyle(
                    color: Colors.white, fontSize: ScreenUtil().setSp(28)),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Provider.of<CartProvider>(context, listen: false).remove();
            },
            child: Container(
              alignment: Alignment.center,
              width: ScreenUtil().setWidth(320),
              color: Colors.red,
              child: Text(
                '马上购买',
                style: TextStyle(
                    color: Colors.white, fontSize: ScreenUtil().setSp(28)),
              ),
            ),
          )
        ],
      ),
    );
  }
}
