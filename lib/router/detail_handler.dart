import 'package:fluro/fluro.dart';
import 'package:flutter_business_1907a/page/detail/detail_page.dart';

Handler detailHandler =
    Handler(handlerFunc: (context, Map<String, List<String>> params) {
  String goodsId = params['id'].first;
  print('index>details goodsId is $goodsId');
  return DetailsPage(
    goodsId: goodsId,
  );
});
