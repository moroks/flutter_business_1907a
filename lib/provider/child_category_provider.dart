import 'package:flutter/material.dart';
import 'package:flutter_business_1907a/bean/category_child_bean_result.dart';
import 'package:flutter_business_1907a/bean/category_goods_bean_result.dart';

class ChildCategoryProvider with ChangeNotifier {
  List<CategoryChildBean> childCategoryList = [];
  List<CategoryGoodsBean> categoryGoodsList = [];
  int page = 1; //通过页面来判断是否是切换后的第一页,同时可以通过page++来加载更多
  int childId = 14;
  int childIndex = 0; //右侧上方小导航的下标
  int parentIndex = 0; //左侧导航的下标

  changeParentIndex(index) {
    parentIndex = index;
    notifyListeners();
  }

  changeChildIndex(int index) {
    childIndex = index;
    notifyListeners();
  }

  setChildCategory(list) {
    childCategoryList = list;
    notifyListeners();
  }

  setCategoryGoodsList(list) {
    categoryGoodsList = list;
    notifyListeners();
  }

  //增加page的方法
  addPage() {
    page++;
    notifyListeners();
  }

  //设置page归1
  rePage() {
    page = 1;
    notifyListeners();
  }

  addCategoryGoodsList(list) {
    categoryGoodsList.addAll(list);
    notifyListeners();
  }

  setChildId(childId) {
    this.childId = childId;
    notifyListeners();
  }
}
