import 'package:flutter/material.dart';
import 'package:flutter_business_1907a/provider/detail_info_provider.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';

///详情页面快递部分
class DetailExplain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(top: 10.0),
      padding: EdgeInsets.all(10.0),
      width: ScreenUtil().setWidth(750),
      child: Column(
        children: [
          Text(
            '说明: > 急速送达 > 正品保证',
            style:
                TextStyle(color: Colors.red, fontSize: ScreenUtil().setSp(30)),
          ),
          Text(
              '${Provider.of<DetailInfoProvider>(context).goodsInfo.goodsDefaultSku}',
              style: TextStyle(
                  color: Colors.black26, fontSize: ScreenUtil().setSp(20)))
        ],
      ),
    );
  }
}
