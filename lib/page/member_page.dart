import 'package:flutter/material.dart';
import 'package:flutter_business_1907a/provider/counter.dart';
import 'package:provider/provider.dart';

class MemberPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('${Provider.of<Counter>(context).value}'),
      ),
    );
  }
}
