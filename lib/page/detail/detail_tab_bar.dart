import 'package:flutter/material.dart';
import 'package:flutter_business_1907a/provider/detail_info_provider.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';

class DetailTabBar extends StatelessWidget {
  Widget _myTabBarLeft(context, isLeft) {
    return InkWell(
      onTap: () {
        Provider.of<DetailInfoProvider>(context, listen: false)
            .changeLeftAndRight('left');
      },
      child: Container(
        padding: EdgeInsets.all(10.0),
        alignment: Alignment.center,
        width: ScreenUtil().setWidth(375),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              bottom: BorderSide(
                  width: 5.0, color: isLeft ? Colors.pink : Colors.black12),
            )),
        child: Text(
          '详情',
          style: TextStyle(color: isLeft ? Colors.pink : Colors.black),
        ),
      ),
    );
  }

  Widget _myTabBarRight(context, isRight) {
    return InkWell(
      onTap: () {
        Provider.of<DetailInfoProvider>(context, listen: false)
            .changeLeftAndRight('right');
      },
      child: Container(
        padding: EdgeInsets.all(10.0),
        alignment: Alignment.center,
        width: ScreenUtil().setWidth(375),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              bottom: BorderSide(
                  width: 5.0, color: isRight ? Colors.pink : Colors.black12),
            )),
        child: Text(
          '评论',
          style: TextStyle(color: isRight ? Colors.pink : Colors.black),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var isLeft = Provider.of<DetailInfoProvider>(context).isLeft;
    var isRight = Provider.of<DetailInfoProvider>(context).isRight;
    return Container(
      margin: EdgeInsets.only(top: 15.0),
      child: Row(
        children: [
          _myTabBarLeft(context, isLeft),
          _myTabBarRight(context, isRight)
        ],
      ),
    );
  }
}
