import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_business_1907a/provider/detail_info_provider.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';

class DetailTop extends StatelessWidget {
  //商品图片,可替换为轮播图
  Widget _goodsImage(url) {
    return Image.network(
      url,
      width: ScreenUtil().setWidth(740),
    );
  }

  //商品名称
  Widget _goodsName(name) {
    return Container(
      width: ScreenUtil().setWidth(730),
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      child: Text(name,
          maxLines: 3, style: TextStyle(fontSize: ScreenUtil().setSp(30))),
    );
  }

  //商品编号
  Widget _goodsNum(num) {
    return Container(
      width: ScreenUtil().setWidth(730),
      padding: EdgeInsets.only(left: 15.0),
      margin: EdgeInsets.only(top: 8.0),
      child: Text(
        '编号:$num',
        style: TextStyle(color: Colors.black26),
      ),
    );
  }

  //商品价格
  Widget _goodsPrice(normalPrice, groupPrice) {
    return Container(
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      margin: EdgeInsets.only(top: 8.0),
      child: Row(
        children: [
          Text(
            '团购价:¥$groupPrice元',
            style:
                TextStyle(color: Colors.pink, fontSize: ScreenUtil().setSp(40)),
          ),
          Text(
            '原价:¥$normalPrice元',
            style: TextStyle(
                color: Colors.black26, decoration: TextDecoration.lineThrough),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, height: 1334, width: 750);
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(2.0),
      child: Column(
        children: [
          _goodsImage(Provider.of<DetailInfoProvider>(context)
              .goodsInfo
              .goodsDefaultIcon),
          _goodsName(Provider.of<DetailInfoProvider>(context)
              .goodsInfo
              .goodsDesc),
          _goodsNum(Provider.of<DetailInfoProvider>(context)
              .goodsInfo
              .goodsCode),
          _goodsPrice(
              Provider.of<DetailInfoProvider>(context)
                  .goodsInfo
                  .goodsDefaultPrice,
              Provider.of<DetailInfoProvider>(context)
                  .goodsInfo
                  .goodsDefaultPrice),
        ],
      ),
    );
  }
}
