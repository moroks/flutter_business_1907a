import 'dart:convert' show json;

class BannerbeanResult {

  int code;
  String msg;
  List<Bannerbean> data;

  BannerbeanResult.fromParams({this.code, this.msg, this.data});

  factory BannerbeanResult(jsonStr) => jsonStr == null ? null : jsonStr is String ? new BannerbeanResult.fromJson(json.decode(jsonStr)) : new BannerbeanResult.fromJson(jsonStr);

  BannerbeanResult.fromJson(jsonRes) {
    code = jsonRes['code'];
    msg = jsonRes['msg'];
    data = jsonRes['data'] == null ? null : [];

    for (var dataItem in data == null ? [] : jsonRes['data']){
      data.add(dataItem == null ? null : new Bannerbean.fromJson(dataItem));
    }
  }

  @override
  String toString() {
    return '{"code": $code,"msg": ${msg != null?'${json.encode(msg)}':'null'},"data": $data}';
  }
}

class Bannerbean {

  Object url;
  int id;
  int imgorder;
  int type;
  String imagepath;
  String title;

  Bannerbean.fromParams({this.url, this.id, this.imgorder, this.type, this.imagepath, this.title});

  Bannerbean.fromJson(jsonRes) {
    url = jsonRes['url'];
    id = jsonRes['id'];
    imgorder = jsonRes['imgorder'];
    type = jsonRes['type'];
    imagepath = jsonRes['imagepath'];
    title = jsonRes['title'];
  }

  @override
  String toString() {
    return '{"url": $url,"id": $id,"imgorder": $imgorder,"type": $type,"imagepath": ${imagepath != null?'${json.encode(imagepath)}':'null'},"title": ${title != null?'${json.encode(title)}':'null'}}';
  }
}

