import 'package:dio/dio.dart';
import 'package:flutter_business_1907a/config/service_url.dart';

Future request(url, formData, bool isGoodsList) async {
  try {
    Response response;
    Dio dio = Dio();
    if (formData == null) {
      response = await dio.get(servicePath[url]);
    } else if (isGoodsList) {
      response = await dio.get(servicePath[url] + formData);
    } else {
      if (formData == '4') {
        response = await dio.get(servicePath[url] + formData);
      } else {
        response = await dio.get(servicePath[url], queryParameters: formData);
      }
    }
    if (response.statusCode == 200) {
      return response.data;
    } else {
      throw Exception('后端接口出现异常,请求失败:$url');
    }
  } catch (e) {
    return print('ERROR:==============>$e');
  }
}
