import 'dart:convert' show json;

class HomeNavigatorBeanResult {

  int code;
  String msg;
  List<HomeNavigatorBean> data;

  HomeNavigatorBeanResult.fromParams({this.code, this.msg, this.data});

  factory HomeNavigatorBeanResult(jsonStr) => jsonStr == null ? null : jsonStr is String ? new HomeNavigatorBeanResult.fromJson(json.decode(jsonStr)) : new HomeNavigatorBeanResult.fromJson(jsonStr);

  HomeNavigatorBeanResult.fromJson(jsonRes) {
    code = jsonRes['code'];
    msg = jsonRes['msg'];
    data = jsonRes['data'] == null ? null : [];

    for (var dataItem in data == null ? [] : jsonRes['data']){
      data.add(dataItem == null ? null : new HomeNavigatorBean.fromJson(dataItem));
    }
  }

  @override
  String toString() {
    return '{"code": $code,"msg": ${msg != null?'${json.encode(msg)}':'null'},"data": $data}';
  }
}

class HomeNavigatorBean {

  int attenNews;
  int attienNum;
  int id;
  String attenName;
  String attienImage;
  String createTime;

  HomeNavigatorBean.fromParams({this.attenNews, this.attienNum, this.id, this.attenName, this.attienImage, this.createTime});

  HomeNavigatorBean.fromJson(jsonRes) {
    attenNews = jsonRes['attenNews'];
    attienNum = jsonRes['attienNum'];
    id = jsonRes['id'];
    attenName = jsonRes['attenName'];
    attienImage = jsonRes['attienImage'];
    createTime = jsonRes['createTime'];
  }

  @override
  String toString() {
    return '{"attenNews": $attenNews,"attienNum": $attienNum,"id": $id,"attenName": ${attenName != null?'${json.encode(attenName)}':'null'},"attienImage": ${attienImage != null?'${json.encode(attienImage)}':'null'},"createTime": ${createTime != null?'${json.encode(createTime)}':'null'}}';
  }
}

