import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_business_1907a/page/index_page.dart';
import 'package:flutter_business_1907a/provider/cart_provider.dart';
import 'package:flutter_business_1907a/provider/child_category_provider.dart';
import 'package:flutter_business_1907a/provider/counter.dart';
import 'package:flutter_business_1907a/provider/detail_info_provider.dart';
import 'package:flutter_business_1907a/router/application.dart';
import 'package:provider/provider.dart';
import 'package:flutter_business_1907a/router/router.dart';

///1.创建一个Provide,相当于全局变量
///2.在main中注册这个全局变量(Provider)
///3.使用方法:Provider.of<Counter>(context,listen: false).increment();注意***需要添加泛型才能调用到具体的Provider
///4.调用变量不需要添加****listen****Provider.of<Counter>(context).value
void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => Counter()),
      ChangeNotifierProvider(create: (_) => ChildCategoryProvider()),
      ChangeNotifierProvider(create: (_) => DetailInfoProvider()),
      ChangeNotifierProvider(create: (_) => CartProvider()),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final router = FluroRouter();
    Routers.configureRoutes(router);
    Application.router = router;
    return MaterialApp(
      title: '百姓生活+',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: Colors.pink),
      home: IndexPage(),
      onGenerateRoute: Application.router.generator,
    );
  }
}
