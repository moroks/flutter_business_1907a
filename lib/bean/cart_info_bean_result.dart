class CartInfoBeanResult{
  String goodsName;
  String images;
  int count;
  String price;
  num goodsId;

  CartInfoBeanResult({this.goodsName,this.images,this.count,this.price,this.goodsId});

  CartInfoBeanResult.fromJson(Map<String,dynamic> json){
    this.goodsName = json['goodsName'];
    this.images = json['images'];
    this.count = json['count'];
    this.price = json['price'];
    this.goodsId = json['goodsId'];
  }

  Map<String,dynamic> toJson (){
    final Map<String,dynamic> data  = Map<String,dynamic>();
    data['goodsName']=this.goodsName;
    data['images']=this.images;
    data['count']=this.count;
    data['price']=this.price;
    data['goodsId']=this.goodsId;
    return data;
  }
}