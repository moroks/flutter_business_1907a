import 'package:flutter/material.dart';
import 'package:flutter_business_1907a/bean/detail_goods_bean_result.dart';

class DetailInfoProvider with ChangeNotifier {
  DetailGoodsBean goodsInfo;
  bool isLeft = true;
  bool isRight = false;

  setGoodsInfo(goodsInfo) {
    // request('getGoodsDetail',id,true).then((value) => goodsInfo = DetailGoodsBeanResult.fromJson(value).data);
    this.goodsInfo = goodsInfo;
    notifyListeners();
  }

  changeLeftAndRight(String changeState) {
    if (changeState == 'left') {
      isLeft = true;
      isRight = false;
    } else {
      isLeft = false;
      isRight = true;
    }
    notifyListeners();
  }
}
