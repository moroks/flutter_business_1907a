import 'dart:convert' show json;

class HomeHotGoodsResult {

  int code;
  String msg;
  HomeHotGoodsData data;

  HomeHotGoodsResult.fromParams({this.code, this.msg, this.data});

  factory HomeHotGoodsResult(jsonStr) => jsonStr == null ? null : jsonStr is String ? new HomeHotGoodsResult.fromJson(json.decode(jsonStr)) : new HomeHotGoodsResult.fromJson(jsonStr);

  HomeHotGoodsResult.fromJson(jsonRes) {
    code = jsonRes['code'];
    msg = jsonRes['msg'];
    data = jsonRes['data'] == null ? null : new HomeHotGoodsData.fromJson(jsonRes['data']);
  }

  @override
  String toString() {
    return '{"code": $code,"msg": ${msg != null?'${json.encode(msg)}':'null'},"data": $data}';
  }
}

class HomeHotGoodsData {

  int endRow;
  int navigateFirstPage;
  int navigateLastPage;
  int navigatePages;
  int nextPage;
  int pageNum;
  int pageSize;
  int pages;
  int prePage;
  int size;
  int startRow;
  int total;
  bool hasNextPage;
  bool hasPreviousPage;
  bool isFirstPage;
  bool isLastPage;
  List<HomeHotGoodsBean> list;
  List<int> navigatepageNums;

  HomeHotGoodsData.fromParams({this.endRow, this.navigateFirstPage, this.navigateLastPage, this.navigatePages, this.nextPage, this.pageNum, this.pageSize, this.pages, this.prePage, this.size, this.startRow, this.total, this.hasNextPage, this.hasPreviousPage, this.isFirstPage, this.isLastPage, this.list, this.navigatepageNums});

  HomeHotGoodsData.fromJson(jsonRes) {
    endRow = jsonRes['endRow'];
    navigateFirstPage = jsonRes['navigateFirstPage'];
    navigateLastPage = jsonRes['navigateLastPage'];
    navigatePages = jsonRes['navigatePages'];
    nextPage = jsonRes['nextPage'];
    pageNum = jsonRes['pageNum'];
    pageSize = jsonRes['pageSize'];
    pages = jsonRes['pages'];
    prePage = jsonRes['prePage'];
    size = jsonRes['size'];
    startRow = jsonRes['startRow'];
    total = jsonRes['total'];
    hasNextPage = jsonRes['hasNextPage'];
    hasPreviousPage = jsonRes['hasPreviousPage'];
    isFirstPage = jsonRes['isFirstPage'];
    isLastPage = jsonRes['isLastPage'];
    list = jsonRes['list'] == null ? null : [];

    for (var listItem in list == null ? [] : jsonRes['list']){
      list.add(listItem == null ? null : new HomeHotGoodsBean.fromJson(listItem));
    }

    navigatepageNums = jsonRes['navigatepageNums'] == null ? null : [];

    for (var navigatepageNumsItem in navigatepageNums == null ? [] : jsonRes['navigatepageNums']){
      navigatepageNums.add(navigatepageNumsItem);
    }
  }

  @override
  String toString() {
    return '{"endRow": $endRow,"navigateFirstPage": $navigateFirstPage,"navigateLastPage": $navigateLastPage,"navigatePages": $navigatePages,"nextPage": $nextPage,"pageNum": $pageNum,"pageSize": $pageSize,"pages": $pages,"prePage": $prePage,"size": $size,"startRow": $startRow,"total": $total,"hasNextPage": $hasNextPage,"hasPreviousPage": $hasPreviousPage,"isFirstPage": $isFirstPage,"isLastPage": $isLastPage,"list": $list,"navigatepageNums": $navigatepageNums}';
  }
}

class HomeHotGoodsBean {

  int categoryId;
  int goodsSalesCount;
  int goodsStockCount;
  int id;
  String goodsBanner;
  String goodsCode;
  String goodsDefaultIcon;
  String goodsDefaultPrice;
  String goodsDefaultSku;
  String goodsDesc;
  String goodsDetailOne;
  String goodsDetailTwo;

  HomeHotGoodsBean.fromParams({this.categoryId, this.goodsSalesCount, this.goodsStockCount, this.id, this.goodsBanner, this.goodsCode, this.goodsDefaultIcon, this.goodsDefaultPrice, this.goodsDefaultSku, this.goodsDesc, this.goodsDetailOne, this.goodsDetailTwo});

  HomeHotGoodsBean.fromJson(jsonRes) {
    categoryId = jsonRes['categoryId'];
    goodsSalesCount = jsonRes['goodsSalesCount'];
    goodsStockCount = jsonRes['goodsStockCount'];
    id = jsonRes['id'];
    goodsBanner = jsonRes['goodsBanner'];
    goodsCode = jsonRes['goodsCode'];
    goodsDefaultIcon = jsonRes['goodsDefaultIcon'];
    goodsDefaultPrice = jsonRes['goodsDefaultPrice'];
    goodsDefaultSku = jsonRes['goodsDefaultSku'];
    goodsDesc = jsonRes['goodsDesc'];
    goodsDetailOne = jsonRes['goodsDetailOne'];
    goodsDetailTwo = jsonRes['goodsDetailTwo'];
  }

  @override
  String toString() {
    return '{"categoryId": $categoryId,"goodsSalesCount": $goodsSalesCount,"goodsStockCount": $goodsStockCount,"id": $id,"goodsBanner": ${goodsBanner != null?'${json.encode(goodsBanner)}':'null'},"goodsCode": ${goodsCode != null?'${json.encode(goodsCode)}':'null'},"goodsDefaultIcon": ${goodsDefaultIcon != null?'${json.encode(goodsDefaultIcon)}':'null'},"goodsDefaultPrice": ${goodsDefaultPrice != null?'${json.encode(goodsDefaultPrice)}':'null'},"goodsDefaultSku": ${goodsDefaultSku != null?'${json.encode(goodsDefaultSku)}':'null'},"goodsDesc": ${goodsDesc != null?'${json.encode(goodsDesc)}':'null'},"goodsDetailOne": ${goodsDetailOne != null?'${json.encode(goodsDetailOne)}':'null'},"goodsDetailTwo": ${goodsDetailTwo != null?'${json.encode(goodsDetailTwo)}':'null'}}';
  }
}

