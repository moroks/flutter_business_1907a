import 'dart:convert' show json;

class DetailGoodsBeanResult {

  int code;
  String msg;
  DetailGoodsBean data;

  DetailGoodsBeanResult.fromParams({this.code, this.msg, this.data});

  factory DetailGoodsBeanResult(jsonStr) => jsonStr == null ? null : jsonStr is String ? new DetailGoodsBeanResult.fromJson(json.decode(jsonStr)) : new DetailGoodsBeanResult.fromJson(jsonStr);

  DetailGoodsBeanResult.fromJson(jsonRes) {
    code = jsonRes['code'];
    msg = jsonRes['msg'];
    data = jsonRes['data'] == null ? null : new DetailGoodsBean.fromJson(jsonRes['data']);
  }

  @override
  String toString() {
    return '{"code": $code,"msg": ${msg != null?'${json.encode(msg)}':'null'},"data": $data}';
  }
}

class DetailGoodsBean {

  int categoryId;
  int goodsSalesCount;
  int goodsStockCount;
  int id;
  String goodsBanner;
  String goodsCode;
  String goodsDefaultIcon;
  String goodsDefaultPrice;
  String goodsDefaultSku;
  String goodsDesc;
  String goodsDetailOne;
  String goodsDetailTwo;

  DetailGoodsBean.fromParams({this.categoryId, this.goodsSalesCount, this.goodsStockCount, this.id, this.goodsBanner, this.goodsCode, this.goodsDefaultIcon, this.goodsDefaultPrice, this.goodsDefaultSku, this.goodsDesc, this.goodsDetailOne, this.goodsDetailTwo});

  DetailGoodsBean.fromJson(jsonRes) {
    categoryId = jsonRes['categoryId'];
    goodsSalesCount = jsonRes['goodsSalesCount'];
    goodsStockCount = jsonRes['goodsStockCount'];
    id = jsonRes['id'];
    goodsBanner = jsonRes['goodsBanner'];
    goodsCode = jsonRes['goodsCode'];
    goodsDefaultIcon = jsonRes['goodsDefaultIcon'];
    goodsDefaultPrice = jsonRes['goodsDefaultPrice'];
    goodsDefaultSku = jsonRes['goodsDefaultSku'];
    goodsDesc = jsonRes['goodsDesc'];
    goodsDetailOne = jsonRes['goodsDetailOne'];
    goodsDetailTwo = jsonRes['goodsDetailTwo'];
  }

  @override
  String toString() {
    return '{"categoryId": $categoryId,"goodsSalesCount": $goodsSalesCount,"goodsStockCount": $goodsStockCount,"id": $id,"goodsBanner": ${goodsBanner != null?'${json.encode(goodsBanner)}':'null'},"goodsCode": ${goodsCode != null?'${json.encode(goodsCode)}':'null'},"goodsDefaultIcon": ${goodsDefaultIcon != null?'${json.encode(goodsDefaultIcon)}':'null'},"goodsDefaultPrice": ${goodsDefaultPrice != null?'${json.encode(goodsDefaultPrice)}':'null'},"goodsDefaultSku": ${goodsDefaultSku != null?'${json.encode(goodsDefaultSku)}':'null'},"goodsDesc": ${goodsDesc != null?'${json.encode(goodsDesc)}':'null'},"goodsDetailOne": ${goodsDetailOne != null?'${json.encode(goodsDetailOne)}':'null'},"goodsDetailTwo": ${goodsDetailTwo != null?'${json.encode(goodsDetailTwo)}':'null'}}';
  }
}

