import 'dart:convert' show json;

class CategoryChildBeanResult {
  int code;
  String msg;
  List<CategoryChildBean> data;

  CategoryChildBeanResult.fromParams({this.code, this.msg, this.data});

  factory CategoryChildBeanResult(jsonStr) => jsonStr == null
      ? null
      : jsonStr is String
          ? new CategoryChildBeanResult.fromJson(json.decode(jsonStr))
          : new CategoryChildBeanResult.fromJson(jsonStr);

  CategoryChildBeanResult.fromJson(jsonRes) {
    code = jsonRes['code'];
    msg = jsonRes['msg'];
    data = jsonRes['data'] == null ? null : [];

    for (var dataItem in data == null ? [] : jsonRes['data']) {
      data.add(
          dataItem == null ? null : new CategoryChildBean.fromJson(dataItem));
    }
  }

  @override
  String toString() {
    return '{"code": $code,"msg": ${msg != null ? '${json.encode(msg)}' : 'null'},"data": $data}';
  }
}

class CategoryChildBean {
  int id;
  int parentId;
  String categoryIcon;
  String categoryName;

  CategoryChildBean.fromParams(
      {this.id, this.parentId, this.categoryIcon, this.categoryName});

  CategoryChildBean.fromJson(jsonRes) {
    id = jsonRes['id'];
    parentId = jsonRes['parentId'];
    categoryIcon = jsonRes['categoryIcon'];
    categoryName = jsonRes['categoryName'];
  }

  @override
  String toString() {
    return '{"id": $id,"parentId": $parentId,"categoryIcon": ${categoryIcon != null ? '${json.encode(categoryIcon)}' : 'null'},"categoryName": ${categoryName != null ? '${json.encode(categoryName)}' : 'null'}}';
  }
}
